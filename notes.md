1. create a docker network  
   ```
   docker network create --subnet 10.42.42.0/24 grafana
   ```
1. run a baremetal container to simulate a server  
   ```
   docker run -it --rm --network grafana --name server-1 --hostname server-1 -p 19090:9090 -p 18080:8080 -p 18090:8090 -v $(pwd)/configs:/configs -v $(pwd)/rules:/rules sebidude/grafana-agent-workshop:latest
   ```
1. run the stuff
   1. prometheus  
      ```
      export HOSTNAME=server-1
      nohup /usr/local/bin/prometheus \
        --storage.tsdb.retention.time=7d \
        --config.file=/configs/prometheus.yml \
        --storage.tsdb.path=/var/lib/prometheus/data \
        --web.enable-lifecycle \
        --web.enable-remote-write-receiver \
        --web.listen-address=0.0.0.0:9090 \
        --web.page-title="Prometheus $HOSTNAME" &>/tmp/prometheus.log &
      ```
    2. grafana-agent  
       ```
        export AGENT_MODE=flow
        export HOSTNAME=server-1
        nohup /usr/local/bin/grafana-agent run \
            /configs/grafana-agent.conf \
            --storage.path=/var/lib/grafana-agent \
            --cluster.enabled=true \
            --cluster.join-addresses="server-1:8090,server-2:8090" \
            --cluster.advertise-address=$HOSTNAME:8090 \
            --disable-reporting \
            --server.http.listen-addr=0.0.0.0:8090 &>/tmp/agent.log &
       ```
